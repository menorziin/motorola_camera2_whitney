# Motorola Moto Camera 2 (whitney): List 9

<center><img src="https://telegra.ph/file/cf43da1a6481e7c094ff6.jpg"/></center>

Compatible Devices:

- Moto E (2020) (ginna)
- Moto E7 Plus (guam)
- Moto G 5G Plus (nairo)
- Moto G9 (guamp)
- Moto G9 Play (guamp)
- Moto G9 Plus (odessa)
- Motorola Edge (racer)
- Motorola Edge Plus (burton)
- Motorola One Fusion+ (liber)
- Motorola One Fusion (astro)
- Motorola One 5G (nairo)


**How to add it in your tree**

To clone:

`git clone https://gitlab.com/NemesisDevelopers/moto-camera/motorola_camera2_whitney.git -b eleven-arm packages/apps/MotCamera2`

`git clone https://gitlab.com/NemesisDevelopers/moto-camera/motorola_camera2_overlay.git -b ten packages/apps/MotCamera2-overlay`

`git clone https://gitlab.com/NemesisDevelopers/motorola/motorola_motosignatureapp.git -b eleven packages/apps/MotoSignatureApp`

Add this in your dependencies:

```
 {
   "repository": "motorola_camera2_whitney",
   "target_path": "packages/apps/MotCamera2",
   "branch": "eleven-arm",
   "remote": "moto-camera"
 }
```
Add this in your device.mk or common.mk:

```
# Moto Camera 2
PRODUCT_PACKAGES += \
    MotCamera2
```

# [Download & info](https://telegra.ph/Moto-Camera-2-List-N9-05-09)


 Copyright © 2020-2021 Nemesis Team
